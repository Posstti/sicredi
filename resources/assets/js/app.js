
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");

window.Vue = require("vue");

// Importando Vuex para comunicação entre componentes
import Vuex from "vuex";
Vue.use(Vuex);

/* Constante para manipulação de informação entre componentes;
 * Objeto item utilizado para armazenar informação
 * método setItem utilizado para atribuir informação em item
 */
const store = new Vuex.Store({
	state:{
		item: {}
	},
	mutations:{
		setItem(state,obj){
			state.item = obj;
		}
	}
});

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component("box-component", require("./components/BoxComponent.vue"));
Vue.component("bread-crumb-component", require("./components/BreadCrumbComponent.vue"));
Vue.component("checkbox-component", require("./components/CheckboxComponent.vue"));
Vue.component("example-component", require("./components/ExampleComponent.vue"));
Vue.component("form-component", require("./components/FormComponent.vue"));
Vue.component("header-component", require("./components/HeaderComponent.vue"));
Vue.component("page-component", require("./components/PageComponent.vue"));
Vue.component("panel-component", require("./components/PanelComponent.vue"));
Vue.component("table-component", require("./components/TableComponent.vue"));
Vue.component("simple-table-component", require("./components/SimpleTableComponent.vue"));

// Componentes da pasta Modal
Vue.component("modal-component", require("./components/modal/ModalComponent.vue"));
Vue.component("modal-link-component", require("./components/modal/ModalLinkComponent.vue"));

const app = new Vue({
    el: "#app",
    store,
    mounted: function(){
    	$("#app").show();
    }
});
