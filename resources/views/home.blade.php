@extends('layouts.app')

@section('content')
    <page-component>
        <panel-component headline="Dashboard - Cadastros">
            <div class="row">
                <div class="col-md-3">
                    <box-component 
                        total="{{ $totalClients }}" 
                        title="Clientes" 
                        url="{{ route('clientes.index') }}" 
                        color="green" 
                        icon="ion ion-ios-people">
                    </box-component>
                </div>
                <div class="col-md-3">
                    <box-component 
                        total="{{ $totalContactMeans }}" 
                        title="Formas de Contato" 
                        url="{{ route('formas_contato.index') }}" 
                        color="darkorange" 
                        icon="ion ion-folder">
                    </box-component>
                </div>
            </div>
        </panel-component>
        <panel-component headline="Dashboard - Gerenciamento">
            <div class="row">
                <div class="col-md-3">
                    <box-component 
                        total="'" 
                        title="Auditoria de Ações" 
                        url="{{ route('auditoria.index') }}" 
                        color="darkblue" 
                        icon="ion ion-ios-eye">
                    </box-component>
                </div>
                <div class="col-md-3">
                    <box-component 
                        total="'" 
                        title="Monitor" 
                        url="{{ url('/monitor') }}" 
                        color="darkred" 
                        icon="ion ion-monitor">
                    </box-component>
                </div>
            </div>
        </panel-component>
    </page-component>
@endsection
