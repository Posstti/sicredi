@extends('layouts.app')

@section('content')
    <page-component>
        <panel-component headline="Monitor">
            <h3>
                Clientes <span style="font-size:15px;" id="totalClients" class="badge badge-info">Carregando...</span>,
                soma das idades   <span style="font-size:15px;" id="totalAges" class="badge badge-info">Carregando...</span> anos
            </h3>
        </panel-component>
    </page-component>
@endsection

<script src="http://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous">
</script>
<script>
  $( document ).ready(function() {
        //Necessidade de criar uma função que fica monitorando e alterando os valores na view
        setInterval(function(){
            $.ajax({
                url: "/clientes/monitor",
                method: "get",
                success: function(data){
                  document.getElementById('totalClients').innerHTML = data.totalClients;
                  document.getElementById('totalAges').innerHTML = data.totalAges;
              }
            });
        }, 1500);
  });
</script>
