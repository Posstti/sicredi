<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Sicredi') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app" style="display:none;">
        <header-component url="{{ url('/home') }}">

            <!-- Links de autenticação -->
            @guest
                <li><a href="{{ route('login') }}">Login</a></li>
                <li><a href="{{ route('register') }}">Cadastrar-se</a></li>
            @else
                <li style="text-align: center;"><a title="Dashboard" href="{{ url('/home') }}">Dashboard</a></li>
                <li style="text-align: center;"><a title="Clientes" href="{{ url('/clientes') }}">Clientes</a></li>
                <li style="text-align: center;"><a title="Formas de Contato" href="{{ url('/formas_contato') }}">Formas de Contato</a></li>
                <li style="text-align: center;"><a title="Auditoria" href="{{ url('/auditoria') }}">Auditoria de Ações</a></li>
                <li style="text-align: center;"><a title="Monitor" href="{{ url('/monitor') }}">Monitor</a></li>
                
                <li class="dropdown">
                    <a href="#" title="Usuário Logado" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu">
                        <li>
                            <a title="Sair" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                                Sair (Logout)
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li>
            @endguest
        </header-component>

        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
