@extends('layouts.app')

@section('content')
    <page-component>
        <panel-component headline="Auditoria de Ações">
        	<a target="_blank" href="/auditoria/relatorio"><button class="btn btn-primary">Ver em PDF</button></a>
        	<simple-table-component
         		v-bind:fields="['Usuário - ID','Usuário - Login','Data','Local','Ação','ID do Registro']"
        		v-bind:items="{{ $data }}">
        	</simple-table-component>
        </panel-component>
    </page-component>

@endsection
