<p>1) Criei projeto Laravel com Composer [version 1.6.5]: Versão do Laravel 5.5.* </p>
2) PHP 7.0.30
3) No arquivo .env o login e senha do mysql está como root | root
4) Utilizei Bootsrap, Javascript, jQuery e Vue.js
5) Para comunicação entre componentes Vue.js utilizei o VueX no projeto, instalado apartir do node.
6) Necessidade de executar o projeto com acesso a internet, pois utilizei ícones do Ionic, através de uma referêcia externa CDN.
7) Outra referência CDN foi de estilo, precisei para o relatório de auditoria de ações (gerar PDF). Infelizmente em alguns momentos ele gera o PDF sem estilo da tabela, porém na grande maioria das vezes um F5 resolveu.