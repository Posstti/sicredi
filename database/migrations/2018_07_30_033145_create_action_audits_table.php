<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActionAuditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('action_audits', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('user_login');
            $table->dateTime('date');
            $table->string('local');
            $table->string('action');
            $table->integer('registry_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('action_audits');
    }
}
